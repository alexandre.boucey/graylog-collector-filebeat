FROM docker.elastic.co/beats/filebeat-oss:7.9.3

USER root

RUN curl -sL https://github.com/just-containers/s6-overlay/releases/download/v2.1.0.2/s6-overlay-amd64-installer > /tmp/s6-overlay-amd64-installer

RUN chmod +x /tmp/s6-overlay-amd64-installer && /tmp/s6-overlay-amd64-installer /

# Define default environment values
ENV GS_SERVER_URL= \
    GS_SERVER_API_TOKEN= \
    GS_NODE_ID=

    
RUN rpm -Uvh https://packages.graylog2.org/repo/packages/graylog-sidecar-repository-1-2.noarch.rpm && \
    yum install -y graylog-sidecar && \
    yum clean all && \
    rm -rf /var/cache/yum

ADD rootfs /

RUN mkdir -p /usr/share/filebeat/bin && ln -s /usr/share/filebeat/filebeat /usr/share/filebeat/bin/filebeat

ENTRYPOINT ["/init"]
CMD []